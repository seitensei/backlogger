import { userInfo } from 'os';
import { Entity, OneToMany, OneToOne, PrimaryGeneratedColumn } from 'typeorm';
import { IInventory } from '../interfaces/IInventory';
import { InventoryRecord } from './InventoryRecord';
import { User } from './User';

@Entity()
export class Inventory implements IInventory {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @OneToMany(() => InventoryRecord, (record) => record.inventory)
    records: InventoryRecord[];

    @OneToOne(() => User, (user) => user.inventory)
    user: User;
}
