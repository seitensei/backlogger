import 'reflect-metadata';
import config from './config';
import express from 'express';
import { createConnection } from 'typeorm';

const init = async () => {
    createConnection().then((connection) => {
        const app = express();
        app.listen(config.port, () => {
            console.log(`Starting server on port ${config.port}`);
        }).on(`error`, (err) => {
            console.error(err);
            process.exit(1);
        });
    });
};

init();
